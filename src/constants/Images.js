// Image
const appIntro1 = require ('../../assets/images/AppIntro1.png');
const appIntro2 = require ('../../assets/images/Room1.png');
const appIntro3 = require ('../../assets/images/AppIntro3.png');
const signImg = require ('../../assets/images/Image.png');
const profile = require ('../../assets/images/nandy.png');
const Temple1 = require ('../../assets/images/KasiViswanatharTemple.png');
const TempleIcon = require ('../../assets/images/TempleIcon.png');


export default {appIntro1, appIntro2, appIntro3,signImg, profile,Temple1,TempleIcon};
