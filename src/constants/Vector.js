/* eslint-disable react-native/no-inline-styles */
// Vector Icons
import React from 'react';
import {COLORS, SIZES} from './Theme';
import MenuIcon from 'react-native-vector-icons/Ionicons';
import NotificationIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import BackIcon from 'react-native-vector-icons/AntDesign';
import UpArrowIcon from 'react-native-vector-icons/AntDesign';
import BackArrowIcon from 'react-native-vector-icons/AntDesign';
import DownArrowIcon from 'react-native-vector-icons/AntDesign';
import HeaderBackArrowIcon from 'react-native-vector-icons/AntDesign';
import DrawerRightIcon from 'react-native-vector-icons/AntDesign';
import CancelIcon from 'react-native-vector-icons/Feather';
import FoodIcon from 'react-native-vector-icons/Ionicons';
import WIFIIcon from 'react-native-vector-icons/AntDesign';
import WaterIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import ParkingIcon from 'react-native-vector-icons/FontAwesome5';
import PersonIcon from 'react-native-vector-icons/Fontisto';
import FB from 'react-native-vector-icons/Fontisto';
import G from 'react-native-vector-icons/AntDesign';
import TwitterIcon from 'react-native-vector-icons/AntDesign';
import EyeIcon from "react-native-vector-icons/Ionicons";
import EyeOffIcon from "react-native-vector-icons/Ionicons";
import PinIcon from "react-native-vector-icons/FontAwesome";
import PersonPinIcon from "react-native-vector-icons/MaterialIcons";
import HotelIcon from "react-native-vector-icons/Fontisto";
import SearchIcon from "react-native-vector-icons/AntDesign";
import Location from "react-native-vector-icons/Ionicons";
import LocationPin from "react-native-vector-icons/MaterialIcons";
import DownIcon from 'react-native-vector-icons/AntDesign';
import ClockIcon from 'react-native-vector-icons/MaterialCommunityIcons';

const MenuImage = (
  <MenuIcon name={'ios-menu-sharp'} color={COLORS.whiteColor} size={25} />
);

const NotificationImage = (
  <NotificationIcon name={'bell'} color={COLORS.whiteColor} size={22} />
);

const Back = (
  <BackIcon name={'left'} color={COLORS.darkGrey} size={SIZES.icon} />
);

const BackArrow = (
  <BackArrowIcon name={'arrowleft'} color={COLORS.blackColor} size={25} />
);
const BackArrow2 = (
  <BackArrowIcon name={'arrowleft'} color={COLORS.lightGrey} size={25} />
);

const HeaderBackArrow = (
  <HeaderBackArrowIcon name={'arrowleft'} color={COLORS.whiteColor} size={25} />
);

const DrawerRight = (
  <DrawerRightIcon name={'right'} color={COLORS.darkGrey} size={20} />
);

const CancelX = <CancelIcon name={'x'} color={COLORS.darkGrey} size={20} />;

const DownArrow = (
  <DownArrowIcon name={'down'} color={COLORS.primaryColor} size={20} />
);

const UpArrow = <UpArrowIcon name={'up'} color={COLORS.darkGrey} size={23} />;

const FaceBook = <FB name={'facebook'} color={COLORS.whiteColor} size={23} />;

const Google = <G name={'google'} color={COLORS.primaryColor} size={23} />;

const Twitter = <TwitterIcon name={'twitter'} color={COLORS.primaryColor} size={23} />;

const Food = <FoodIcon name={'ios-fast-food-outline'} color={COLORS.primaryColor} size={18}/>;

const Wifi = <WIFIIcon name={'wifi'} color={COLORS.primaryColor} size={15}/>;

const Parking = <ParkingIcon name={'parking'} color={COLORS.darkGrey} size={15}/>;

const Water = <WaterIcon name={'cup-water'} color={COLORS.darkGrey} size={15}/>;

const Person = <PersonIcon name={'person'} color={COLORS.darkGrey} size={15}/>;

const Eye = <EyeIcon name={"eye"} color={COLORS.darkGrey} size={25} />;

const EyeOff = (
  <EyeOffIcon name={"eye-off"} color={COLORS.darkGrey} size={25} />
);

const Pin = <PinIcon name={"map-pin"} color={COLORS.primaryColor} size={35}/>

const PersonPin = <PersonPinIcon name={"person-pin"} color={COLORS.blackColor} size={30}/>

const Hotel = <HotelIcon name={"hotel-alt"} color={COLORS.blackColor} size={50}/>

const Search = <SearchIcon name={"search1"} color={COLORS.whiteColor} size={25}/>

const LocationIcon = <Location name={'md-location-sharp'} color={COLORS.primaryColor} size={35}/>

const Map = <LocationPin name={'location-pin'} color={COLORS.primaryColor} size={35}/>

const Down = <DownIcon name={'caretdown'} color={COLORS.primaryColor} size={15}/>

const Clock = <ClockIcon name={'clock'} color={COLORS.blackColor} size={20}/>


export default {
  Back,
  MenuImage,
  NotificationImage,
  BackArrow,
  BackArrow2,
  DrawerRight,
  HeaderBackArrow,
  CancelX,
  DownArrow,
  UpArrow,
  FaceBook,
  Google,
  Twitter,
  Food,
  Wifi,
  Parking,
  Water,
  Person,
  Eye,
  EyeOff,
  Pin,
  PersonPin,
  Hotel,
  Search,
  LocationIcon,
  Map,
  Down,
  Clock,
};
