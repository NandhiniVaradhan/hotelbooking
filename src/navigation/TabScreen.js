import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {ScaledSheet} from 'react-native-size-matters';
import HomeIcon from 'react-native-vector-icons/Entypo';
import MapIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import {COLORS, Fonts, SIZES} from '../constants';
import MapPage from '../screens/location/MapPage';
import RoomDetails from '../screens/detailPage/RoomDetails';
import Home from '../screens/dashboard/Home';
import TermsandConditions
  from '../screens/termsAndConditions/TermsandConditions';
import PrivacyPolicy from '../screens/privacyPolicy/PrivacyPolicy';
import Search from '../screens/search/Search';
import {RoomDescription} from '../screens/detailPage/RoomDescription';
import CommonHeader from '../components/header/CommonHeader';
import CustomDrawer from './CustomDrawer';

const Tab = createBottomTabNavigator ();
const Drawer = createDrawerNavigator ();
const HomeStack = createNativeStackNavigator ();
const MapStack = createNativeStackNavigator ();
const MainStack = createNativeStackNavigator ();

const TabScreen = ({}) => {
  return (
    <Tab.Navigator
      initialRouteName="HomeScreen"
      screenOptions={{
        tabBarStyle: styles.TabStyleBar,
        tabBarLabelStyle: styles.TabLabelStyle,
      }}
    >
      <Tab.Screen
        name={'HomeScreen'}
        component={HomeScreen}
        options={{
          headerShown: false,
          tabBarLabel: 'Home',
          tabBarActiveTintColor: COLORS.primaryColor,
          tabBarIcon: ({color}) => (
            <HomeIcon name="home" color={color} size={26} />
          ),
        }}
      />

      <Tab.Screen
        name={'MapStackScreen'}
        component={MapStackScreen}
        options={{
          headerShown: false,
          tabBarLabel: 'Map',
          tabBarActiveTintColor: COLORS.primaryColor,
          tabBarIcon: ({color}) => (
            <MapIcon name="google-maps" color={color} size={26} />
          ),
        }}
      />

    </Tab.Navigator>
  );
};

const DrawerScreen = ({}) => {
  return (
    <Drawer.Navigator
      drawerContent={props => <CustomDrawer {...props} />}
      initialRouteName="DrawerScreen"
    >
      <Drawer.Screen
        name="DrawerScreen"
        component={MainScreens}
        options={{headerShown: false}}
      />
    </Drawer.Navigator>
  );
};

const HomeScreen = ({navigation}) => {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen
        name={'Home'}
        component={Home}
        options={{
          headerShown: true,
          header: () => (
            <CommonHeader
              type={1}
              onClick={() => navigation.openDrawer ()}
              searchNavigation={() => navigation.navigate ('Search')}
            />
          ),
        }}
      />
    </HomeStack.Navigator>
  );
};

const MapStackScreen = ({navigation}) => {
  return (
    <MapStack.Navigator>
      <MapStack.Screen
        name={'Map'}
        component={MapPage}
        options={{
          headerShown: true,
          header: () => (
            <CommonHeader type={3} onClick={() => navigation.openDrawer ()} />
          ),
        }}
      />
    </MapStack.Navigator>
  );
};

const MainScreens = ({navigation}) => {
  return (
    <MainStack.Navigator>
      <MainStack.Screen
        name={'MainScreens'}
        component={TabScreen}
        options={{
          headerShown: false,
        }}
      />
      <MainStack.Screen
        name={'RoomDetails'}
        component={RoomDetails}
        options={{
          headerShown: true,
          header: () => (
            <CommonHeader
              type={2}
              onClick={() => navigation.goBack ()}
              title={'RoomDetails'}
            />
          ),
        }}
      />
      <MainStack.Screen
        name={'TermsandConditions'}
        component={TermsandConditions}
        options={{
          headerShown: true,
          header: () => (
            <CommonHeader
              type={2}
              onClick={() => navigation.goBack ()}
              title={'Terms and Conditions'}
            />
          ),
        }}
      />

      <MainStack.Screen
        name={'PrivacyPolicy'}
        component={PrivacyPolicy}
        options={{
          headerShown: true,
          header: () => (
            <CommonHeader
              type={2}
              onClick={() => navigation.goBack ()}
              title={'Privacy Policy'}
            />
          ),
        }}
      />

      <MainStack.Screen
        name={'Search'}
        component={Search}
        options={{
          headerShown: true,
          header: () => (
            <CommonHeader
              type={2}
              onClick={() => navigation.goBack ()}
              title={'Search'}
            />
          ),
        }}
      />

      <MainStack.Screen
        name={'RoomDescription'}
        component={RoomDescription}
        options={{headerShown: false}}
      />
    </MainStack.Navigator>
  );
};

const styles = ScaledSheet.create ({
  TabLabelStyle: {fontSize: '11@s', fontFamily: Fonts.Regular},
  TabStyleBar: {
    position: 'absolute',
    backgroundColor: COLORS.whiteColor,
    height: SIZES.height / 13,
  },
  TabFocusBar: {
    height: '4.5@vs',
    width: '50@msr',
    backgroundColor: COLORS.primaryColor,
    borderBottomLeftRadius: '20@msr',
    borderBottomRightRadius: '20@msr',
    top: '-4@vs',
  },
  TabParentView: {
    alignItems: 'center',
  },
  TabMarginTop: {marginTop: '5@vs'},
  image: {
    width: '20@s',
    height: '20@vs',
  },
});
export {TabScreen, DrawerScreen};
