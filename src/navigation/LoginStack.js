import React,{useState,useEffect} from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import AsyncStorage from '@react-native-async-storage/async-storage';
import AppIntroSlider from '../screens/authentication/AppIntroSlider';
import Login from '../screens/authentication/Login';
import Signup from '../screens/authentication/Signup';
const Stack = createNativeStackNavigator ();
const LoginStack = () => {
  const [appIntro, setAppIntro] = useState(null);
  let routeName;
  useEffect(() => {
    AsyncStorage.getItem('alreadyLaunched').then(value => {
      if (value == null) {
        AsyncStorage.setItem('alreadyLaunched', 'true');
        setAppIntro(true);
      } else {
        setAppIntro(false);
      }
    });
  }, []);
  if (appIntro === null) {
    return null;
  } else if (appIntro == true) {
    routeName = 'AppIntroSlider';
  } else {
    routeName = 'Login';
  }
  return (
    <Stack.Navigator initialRouteName={routeName}>
      <Stack.Screen
        name="AppIntroSlider"
        component={AppIntroSlider}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Signup"
        component={Signup}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};
export default LoginStack;
