import React, {useState, useContext} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  Linking,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import auth from '@react-native-firebase/auth';
import {COLORS, FONTS, adjust, Vector} from '../constants';
import {isIphoneX} from '../utils';
import {CommonPopup} from '../components/modal/CommonPopup';
import {AuthContext} from '../navigation/AuthProvider';

const FlatListData = [
  {
    id: 1,
    title: 'Privacy Policy',
  },
  {
    id: 2,
    title: 'Terms and Conditions',
  },
  {
    id: 3,
    title: 'Support',
  },
  {
    id: 4,
    title: 'Log out',
  },
];

const CustomDrawer = props => {
  const [logoutAction, setLogoutAction] = useState (false);

  const clickAction = item => {
    props.navigation.closeDrawer ();
    if (item.id == 1) {
      props.navigation.navigate ('PrivacyPolicy');
    } else if (item.id == 2) {
      props.navigation.navigate ('TermsandConditions');
    } else if (item.id == 3) {
      Linking.openURL ('https://api.whatsapp.com/send?phone=+917338833496');
    } else if (item.id == 4) {
      setLogoutAction (true);
    }
  };

  const {user, logout} = useContext (AuthContext);
  const name = auth().currentUser.displayName;

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: COLORS.backgroundColour}}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.view}>
          <TouchableOpacity onPress={() => props.navigation.closeDrawer ()}>
            {Vector.BackArrow}
          </TouchableOpacity>
          <Text style={styles.title}>Profile</Text>
        </View>
        <View style={styles.profileView}>
          <View style={styles.imgView}>
            <View
              style={[
                styles.imgStyle,
                {
                  backgroundColor: COLORS.primaryColor,
                  justifyContent: 'center',
                },
              ]}
            >
              <Text
                style={{
                  color: COLORS.whiteColor,
                  alignSelf: 'center',
                  fontSize: adjust (40),
                }}
              >
                {name === null ? 'H' : name.slice (0, 1)}
              </Text>
            </View>
            <View style={{marginHorizontal: 15, justifyContent: 'center'}}>
              <Text
                numberOfLines={1}
                style={[styles.nameTitle, {...FONTS.body2}]}
              >
                {auth().currentUser.displayName}
              </Text>
              <Text style={[styles.nameTitle, {...FONTS.body4, marginTop: 2}]}>
                {auth().currentUser.email}
              </Text>
            </View>
          </View>
          <View>
            {FlatListData.map ((item, index) => {
              return (
                <TouchableOpacity
                  key={item.id}
                  activeOpacity={0.3}
                  style={styles.flatListParentView}
                  onPress={() => clickAction (item)}
                >
                  <View style={{flex: 0.8, justifyContent: 'center'}}>
                    <Text numberOfLines={1} style={styles.tabTitle}>
                      {item.title}
                    </Text>
                  </View>
                </TouchableOpacity>
              );
            })}
          </View>
        </View>
      </ScrollView>
      <CommonPopup
        type={0}
        title={'Logout'}
        subtitle={'Are you sure want to logout?'}
        isVisible={logoutAction}
        successButtonText={'Logout'}
        negativeButtonText={'Cancel'}
        onPressNo={() => setLogoutAction (false)}
        onPressYes={() => logout ()}
      />
    </SafeAreaView>
  );
};

const styles = ScaledSheet.create ({
  view: {
    height: isIphoneX () ? '50@vs' : '46@vs',
    marginHorizontal: '10@s',
    alignItems: 'center',
    flexDirection: 'row',
  },
  title: {
    ...FONTS.h2,
    marginStart: '6@s',
    fontSize: adjust (20),
    color: COLORS.blackColor,
  },
  imgStyle: {
    height: '100@vs',
    width: '100@s',
    alignSelf: 'center',
    borderRadius: '150@msr',
  },
  profileView: {
    paddingHorizontal: '15@s',
  },
  imgView: {
    borderBottomWidth: 0.6,
    borderBottomColor: COLORS.secondaryColor,
    paddingVertical: '20@vs',
  },
  nameTitle: {
    color: COLORS.blackColor,
    alignSelf: 'center',
    marginTop: '10@vs',
  },
  flatListParentView: {
    flexDirection: 'row',
    paddingVertical: '15@vs',
    borderBottomWidth: 0.6,
    borderBottomColor: COLORS.secondaryColor,
  },
  tabTitle: {
    ...FONTS.body2,
    fontSize: adjust (14),
    color: COLORS.blackColor,
  },
});

export default CustomDrawer;
