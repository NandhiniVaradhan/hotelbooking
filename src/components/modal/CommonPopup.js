import React from 'react';
import {Text, View, Dimensions, Image, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import {ScaledSheet} from 'react-native-size-matters';
import {FONTS, COLORS, adjust} from '../../constants/Theme';
import CommonButton from '../buttons/CommonButton';
import {Vector} from '../../constants';

var {width} = Dimensions.get ('window');

export const CommonPopup = ({
  isVisible,
  title,
  subtitle,
  onPressNo,
  onPressYes,
  successButtonText,
  negativeButtonText,
  type,
  onClose,
  image,
  onPress,
  total,
}) => {
  return (
    <Modal
      animationIn={'slideInUp'}
      isVisible={isVisible}
      style={styles.container}
    >
      {type === 0
        ? <View style={styles.containerStyle}>
            <Text style={styles.order}>{title}</Text>
            <Text style={styles.order_2}>{subtitle}{total}</Text>
            <View style={[styles.buttonContainer, {marginTop: 20}]}>
              <CommonButton
                buttonStyle={styles.ButtonStyle_No}
                isLoading={false}
                text={negativeButtonText}
                textStyle={styles.buttonTextStyle_No}
                onPress={onPressNo}
              />
              <CommonButton
                buttonStyle={styles.ButtonStyle_yes}
                isLoading={false}
                text={successButtonText}
                textStyle={styles.buttonTextStyle_yes}
                onPress={onPressYes}
              />
            </View>
          </View>
        : type === 1
            ? <View style={styles.containerStyle_1}>
                <Text style={styles.order}>{title}</Text>
                <TouchableOpacity
                  style={{position: 'absolute', top: 10, right: 10}}
                  onPress={onClose}
                >
                  {Vector.CancelX}
                </TouchableOpacity>
                <View style={{paddingVertical: 10}}>
                  <View>
                    <Image
                      source={image}
                      resizeMode={'cover'}
                      style={{
                        height: 150,
                        width: '90%',
                        alignSelf: 'center',
                        marginVertical: 5,
                      }}
                    />
                  </View>
                  <View style={{justifyContent: 'center', marginTop: 5}}>
                    <Text style={styles.order_2}>{subtitle}</Text>
                  </View>
                </View>
                <View
                  style={[
                    styles.buttonContainer,
                    {justifyContent: 'flex-end', marginVertical: 5},
                  ]}
                >
                  <CommonButton
                    buttonStyle={styles.ButtonStyle_yes}
                    isLoading={false}
                    text={'Direction'}
                    textStyle={styles.buttonTextStyle_yes}
                    onPress={onPress}
                  />
                </View>
              </View>
            : null}
    </Modal>
  );
};
const styles = ScaledSheet.create ({
  containerStyle: {
    width: width - 30,
    backgroundColor: 'white',
    borderRadius: '4@msr',
    justifyContent: 'center',
    alignSelf: 'center',
    paddingVertical: '12@msr',
  },
  containerStyle_1: {
    width: width - 30,
    backgroundColor: 'white',
    borderRadius: '4@msr',
    justifyContent: 'center',
    alignSelf: 'center',
    paddingVertical: '12@msr',
  },
  container: {
    height: '600@vs',
  },
  order: {
    ...FONTS.h3,
    fontWeight: '700',
    paddingHorizontal: '10@msr',
    paddingVertical: '5@vs',
    color: COLORS.blackColor,
  },
  order_2: {
    ...FONTS.body3,
    paddingTop: '5@msr',
    paddingHorizontal: '10@msr',
    color: COLORS.blackColor,
  },
  buttonContainer: {
    flexDirection: 'row',
  },
  //buttons
  //no
  buttonTextStyle_No: {
    ...FONTS.P0,
    fontSize: adjust (12),
    color: COLORS.whiteColor,
  },
  ButtonStyle_No: {
    height: '40@vs',
    width: '135@s',
    borderRadius: adjust (5),
    backgroundColor: COLORS.secondaryColor,
  },
  //no
  //yes
  buttonTextStyle_yes: {
    ...FONTS.P0,
    fontSize: adjust (12),
    color: COLORS.whiteColor,
  },
  ButtonStyle_yes: {
    height: '40@vs',
    width: '135@s',
    borderRadius: adjust (5),
    backgroundColor: COLORS.primaryColor,
  },
  //yes
  //Buttons
  //Modal 2
  input2: {
    height: '100@vs',
    width: '305@s',
    color: COLORS.blackColor,
    backgroundColor: '#FBFBFB',
    borderColor: '#FFFFFF',
    borderWidth: 0.5,
    borderRadius: '5@msr',
    fontSize: adjust (16),
    textAlign: 'left',
    marginTop: '10@msr',
    alignSelf: 'center',
  },
});
