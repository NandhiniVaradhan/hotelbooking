import React from 'react';
import {View, ActivityIndicator} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {COLORS} from '../../constants';
export default function Loading () {
  return (
    <View style={styles.loadingContainer}>
      <ActivityIndicator size="large" color={COLORS.secondaryColor} />
    </View>
  );
}
const styles = ScaledSheet.create ({
  loadingContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
