import React from 'react';
import {View, Text, Dimensions} from 'react-native';
import {COLORS, Fonts, adjust} from '../../constants';
import {TextInput} from 'react-native-paper';

var {width} = Dimensions.get ('window');

export const TextInputField = ({
  placeHolder,
  label,
  editable = true,
  value = '',
  onChangedText,
  keyboard = 'default',
  error = false,
  errorText,
  maximumLength = 100,
  onPressing,
  autoCapitalize,
  pointerEvents = 'auto',
  width,
  paddingRight,
  secureTextEntry,
}) => {
  return (
    <View>
      <TextInput
        editable={editable}
        keyboardType={keyboard}
        pointerEvents={pointerEvents}
        autoCapitalize={autoCapitalize}
        mode="outlined"
        label={label}
        placeholder={placeHolder}
        outlineColor={COLORS.lightGrey}
        activeOutlineColor={COLORS.primaryColor}
        style={{
          fontFamily: Fonts.Regular,
          backgroundColor: COLORS.whiteColor,
          //height:height,
          width: width,
          paddingRight: paddingRight,
        }}
        error={error}
        maxLength={maximumLength}
        onChangeText={onChangedText}
        value={value}
        onPressIn={onPressing}
        secureTextEntry={secureTextEntry}
      />
      {error == true
        ? <Text
            style={{
              fontFamily: Fonts.Medium,
              fontSize: adjust (10),
              color: COLORS.primaryColor,
              marginTop: 1,
              marginLeft: 5,
              alignItems: 'flex-start',
            }}
          >
            {errorText}
          </Text>
        : null}
    </View>
  );
};
