import React from 'react';
import {Text} from 'react-native';
import SpinnerButton from 'react-native-spinner-button';
// Button
const CommonButton = ({
  text,
  onPress,
  isLoading,
  type,
  disable = false,
  buttonStyle,
  textStyle,
  disabled,
}) => {
  return (
    <SpinnerButton
      buttonStyle={buttonStyle}
      isLoading={isLoading}
      onPress={onPress}
      spinnerType={'MaterialIndicator'}
      disabled={disabled}
    >
      <Text style={textStyle}>{text}</Text>
    </SpinnerButton>
  );
};
export default CommonButton;
