import React from 'react';
import {View, SafeAreaView, Text, TouchableOpacity} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {ScaledSheet} from 'react-native-size-matters';
import {COLORS, FONTS, Vector} from '../../constants';
import {isIphoneX} from '../../utils';

const CommonHeader = ({onClick, type, title, searchNavigation}) => {
  return (
    <SafeAreaView style={Styles.container}>
      <LinearGradient
        colors={[COLORS.primaryColor, COLORS.secondaryColor]}
        start={{x: 1, y: 0}}
        end={{x: 0, y: 1}}
        style={Styles.linearGradient}
      >
        <View style={Styles.firstFlex}>
          <TouchableOpacity style={Styles.menu} onPress={onClick}>
            {type == 1
              ? Vector.MenuImage
              : type == 2 ? Vector.BackArrow : Vector.MenuImage}
          </TouchableOpacity>
          {type == 1 ? null : <Text style={Styles.headerTitle}>{title}</Text>}
        </View>
        <View style={Styles.secondFlex}>
          <TouchableOpacity style={Styles.search} onPress={searchNavigation}>
            {type == 1 ? Vector.Search : null}
          </TouchableOpacity>
        </View>
      </LinearGradient>
    </SafeAreaView>
  );
};

const Styles = ScaledSheet.create ({
  container: {
    backgroundColor: COLORS.primaryColor,
    height: isIphoneX () ? '50@vs' : '46@vs',
  },
  linearGradient: {
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  firstFlex: {
    alignItems: 'flex-start',
    flexDirection: 'row',
  },
  secondFlex: {
    flex: 0.5,
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexDirection: 'row',
  },
  menu: {marginLeft: '15@vs', alignSelf: 'center', justifyContent: 'center'},
  search: {marginRight: '15@vs', alignSelf: 'center', justifyContent: 'center'},
  imageStyle: {
    height: '30@vs',
    width: '48@s',
    resizeMode: 'contain',
    marginLeft: '10@vs',
  },
  headerTitle: {
    ...FONTS.h3,
    color: COLORS.blackColor,
    marginLeft: '10@s',
    justifyContent: 'center',
    alignSelf: 'center',
  },
});

export default CommonHeader;
