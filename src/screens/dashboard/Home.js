import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  SafeAreaView,
  Image,
  FlatList,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import database from '@react-native-firebase/database';
import {ScaledSheet} from 'react-native-size-matters';
import {COLORS, adjust} from '../../constants';
import Loading from '../../components/loader/Loading';

const widthStatic = (Dimensions.get ('window').width - 7 * 10) / 0.9;

const Home = ({navigation}) => {
  const [single, setSingle] = useState ([]);
  const [loading, setLoading] = useState (true);

  useEffect (() => {
    itemsRef.on ('value', snapshot => {
      let data = snapshot.val ();
      const Rooms = Object.values (data);
      setSingle (Rooms);
      setLoading (false);
    });
  }, []);

  let itemsRef = database ().ref (`/Rooms`);

  return (
    <SafeAreaView style={styles.container}>

      {loading
        ? <Loading />
        : <View style={styles.flat}>
            <FlatList
              data={single}
              showsVerticalScrollIndicator={false}
              renderItem={({item}) => (
                <View style={styles.card1}>
                  <TouchableOpacity
                    onPress={() =>
                      navigation.navigate ('RoomDetails', {
                        Room_Image: item.image,
                        Room_Title: item.title,
                        Room_Person: item.person,
                        Room_Price: item.price,
                        Room_Description: item.description,
                      })}
                  >
                    <Image source={{uri: item.image}} style={styles.image} />
                    <View style={styles.card2}>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={styles.title}>
                          {item.title}
                        </Text>
                        <Text style={styles.title1}>
                          - {item.type}
                        </Text>
                      </View>
                      <Text style={styles.price}>
                        {item.price}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              )}
            />
          </View>}
    </SafeAreaView>
  );
};

export default Home;

const styles = ScaledSheet.create ({
  container: {
    flex: 1,
    backgroundColor: COLORS.backgroundColour,
  },
  flat: {
    marginBottom: '54@vs',
  },
  image: {
    alignSelf: 'stretch',
    height: '83%',
    width: '100%',
    borderTopLeftRadius: '20@msr',
    borderTopRightRadius: '20@msr',
  },
  card1: {
    marginTop: '30@vs',
    borderRadius: '20@msr',
    alignSelf: 'center',
    width: widthStatic,
    height: '190@vs',
    backgroundColor: COLORS.whiteColor,
    shadowColor: COLORS.blackColor,
    shadowOpacity: 0.26,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    elevation: 3,
    bottom: 5,
  },
  img: {
    height: '90@vs',
    width: widthStatic,
    borderTopLeftRadius: '4@msr',
    borderTopRightRadius: '4@msr',
    backgroundColor: COLORS.lightGrey,
  },
  card2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: '10@vs',
    alignItems: 'center',
  },
  title: {
    color: COLORS.blackColor,
    fontSize: adjust (15),
    fontWeight: '700',
    marginTop: '5@vs',
  },
  title1: {
    color: COLORS.blackColor,
    fontSize: adjust (10),
    marginTop: '10@vs',
    marginStart: '5@s',
  },
  price: {
    color: COLORS.blackColor,
    fontSize: adjust (12),
    fontWeight: '400',
    marginTop: '10@vs',
  },
});
