import React, {useState} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  StatusBar,
  Dimensions,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {COLORS, FONTS, Images, adjust} from '../../constants';

const AppIntroSlider = ({navigation}) => {
  const [sliderState, setSliderState] = useState ({currentPage: 0});
  const {width, height} = Dimensions.get ('window');

  const setSliderPage = event => {
    const {currentPage} = sliderState;
    const {x} = event.nativeEvent.contentOffset;
    const indexOfNextScreen = Math.floor (x / width);
    if (indexOfNextScreen !== currentPage) {
      setSliderState ({
        ...sliderState,
        currentPage: indexOfNextScreen,
      });
    }
  };

  const {currentPage: pageIndex} = sliderState;

  return (
    <>
      <StatusBar barStyle="light-content" />
      <SafeAreaView style={{flex: 1}}>
        <ScrollView
          style={{flex: 1}}
          horizontal={true}
          scrollEventThrottle={16}
          pagingEnabled={true}
          showsHorizontalScrollIndicator={false}
          onScroll={event => {
            setSliderPage (event);
          }}
        >
          <View style={{width, height}}>
            <ImageBackground
              source={Images.appIntro1}
              style={styles.imageStyle}
            >
              <Text style={styles.title1}>Welcome to Hotel Luxury</Text>
              <Text style={styles.title2}>LUXURY HOTEL IN THE CITY</Text>
            </ImageBackground>
          </View>
          <View style={{width, height}}>
            <ImageBackground
              source={Images.appIntro2}
              style={styles.imageStyle}
            >
              <Text style={styles.title1}>Room Booking</Text>
              <Text style={styles.title2}>Book a Comfortable Room</Text>
            </ImageBackground>
          </View>

          <View style={{width, height}}>
            <ImageBackground
              source={Images.appIntro3}
              style={styles.imageStyle}
            >
              <Text style={styles.title1}>Relax & Enjoy</Text>
              <Text style={styles.title3}>Flat 75% off on Your 1st Room Booking</Text>
              <TouchableOpacity
                style={styles.button}
                onPress={() => navigation.navigate ('Login')}
              >
                <Text
                  style={{
                    color: COLORS.violetColor,
                    fontWeight: 'bold',
                    alignSelf: 'center',
                    ...FONTS.body3,
                  }}
                >
                  GET STARTED
                </Text>
              </TouchableOpacity>
            </ImageBackground>
          </View>
        </ScrollView>
        <View style={styles.paginationWrapper}>
          {Array.from (Array (3).keys ()).map ((key, index) => (
            <View
              style={[
                styles.paginationDots,
                {opacity: pageIndex === index ? 1 : 0.2},
              ]}
              key={index}
            />
          ))}
        </View>
      </SafeAreaView>
    </>
  );
};
export default AppIntroSlider;
const styles = ScaledSheet.create ({
  imageStyle: {
    resizeMode: 'stretch',
    height: '100%',
    width: '100%',
  },
  paginationWrapper: {
    position: 'absolute',
    bottom: '80@vs',
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  paginationDots: {
    height: '10@vs',
    width: '10@s',
    borderRadius: 10 / 2,
    backgroundColor: COLORS.whiteColor,
    marginLeft: '10@s',
  },
  title1: {
    position: 'absolute',
    top: '50@vs',
    alignSelf: 'center',
    fontWeight: '500',
    fontSize: adjust (25),
    color: COLORS.whiteColor,
  },
  title2: {
    position: 'absolute',
    // marginTop: '570@vs',
    bottom: '100@vs',
    alignSelf: 'center',
    fontWeight: '500',
    fontSize: adjust (20),
    color: COLORS.lightGrey,
  },
  title3: {
    position: 'absolute',
    // marginTop: '570@vs',
    bottom: '100@vs',
    alignSelf: 'center',
    fontWeight: '500',
    fontSize: adjust (20),
    color: COLORS.whiteColor,
  },
  button: {
    position: 'absolute',
    alignSelf: 'center',
    // right: '10@s',
    width: '90%',
    bottom: '15@vs',
    paddingVertical: '12@vs',
    backgroundColor: COLORS.whiteColor,
    borderRadius: 10,
  },
});
