import React, {useRef, useState, useContext} from 'react';
import {
  Text,
  View,
  SafeAreaView,
  Image,
  TextInput,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import database from '@react-native-firebase/database';
import {validateEmail, snackBar} from '../../utils';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import auth from '@react-native-firebase/auth';
import {COLORS, FONTS, Vector, Images} from '../../constants';
import Loading from '../../components/loader/Loading';

const Signup = ({navigation}) => {
  const [name, setName] = useState ('');
  const [email, setEmail] = useState ('');
  const [password, setPassword] = useState ('');
  const [secureTextEntry, setSecureTextEntry] = useState (true);
  const [loading, setLoading] = useState (false);

  const input1 = useRef ();
  const input2 = useRef ();
  const input3 = useRef ();

  const UpdatePasswordVisiblity = () => {
    setSecureTextEntry (!secureTextEntry);
  };

  const signup = async (name, email, password) => {
    if (name === '') {
      snackBar ('Username is required');
    } else if (email === '') {
      snackBar ('E-mail address is required');
    } else if (!validateEmail (email)) {
      snackBar ('E-mail address is invalid');
    } else if (password === '') {
      snackBar ('Password is required');
    } else {
      setLoading (true);
      try {
        await auth ()
          .createUserWithEmailAndPassword (email, password)
          .then (async res => {
            const update = {
              displayName: name,
            };
            await res.user.updateProfile (update);

            const reference = database ()
              .ref (`/users/${auth ().currentUser.uid}`)
              .set ({
                name: name,
                email: email,
              });
            setLoading (false);
            snackBar ('Successfully, Registered in');
          });
      } catch (error) {
        if (error.code === 'auth/email-already-in-use') {
          snackBar ('E-mail address is already in use!');
        }
      }
      setLoading (false);
    }
  };

  return (
    <>
      {loading
        ? <Loading />
        : <ScrollView style={styles.container}>

            <SafeAreaView>

              <KeyboardAwareScrollView
                keyboardShouldPersistTaps={'handled'}
                scrollEnabled={true}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{flexGrow: 1}}
              >
                <Image
                  source={Images.signImg}
                  style={styles.image}
                  resizeMode={'cover'}
                />
                <View>
                  <Text style={[styles.title, {marginTop: 20}]}>Signup</Text>
                  <View style={styles.card}>
                    <View style={styles.view}>
                      <Text style={styles.txt}>Name</Text>
                      <TextInput
                        style={styles.inputTxt}
                        onChangeText={text => setName (text)}
                        value={name}
                        placeholder="Enter Username"
                        placeholderTextColor={COLORS.darkGrey}
                        keyboardType="default"
                        borderWidth={1}
                        returnKeyType="next"
                        onSubmitEditing={() => {
                          input2.current.focus ();
                        }}
                        blurOnSubmit={false}
                        ref={input1}
                      />
                    </View>

                    <View style={{marginTop: 20}}>
                      <Text style={styles.txt}>Email</Text>
                      <TextInput
                        style={styles.inputTxt}
                        onChangeText={text => setEmail (text)}
                        value={email}
                        placeholder="Enter Email"
                        placeholderTextColor={COLORS.darkGrey}
                        keyboardType="email-address"
                        autoCapitalize="none"
                        borderWidth={1}
                        returnKeyType="next"
                        onSubmitEditing={() => {
                          input3.current.focus ();
                        }}
                        blurOnSubmit={false}
                        ref={input2}
                      />
                    </View>
                    <View style={{marginTop: 20}}>
                      <Text style={styles.txt}>Password</Text>
                      <View style={{flexDirection: 'row'}}>
                        <TextInput
                          style={styles.inputTxt}
                          onChangeText={text => setPassword (text)}
                          value={password}
                          placeholder="Enter password"
                          secureTextEntry={secureTextEntry ? true : false}
                          placeholderTextColor={COLORS.darkGrey}
                          keyboardType="default"
                          borderWidth={1}
                          returnKeyType="done"
                          ref={input3}
                        />
                        <TouchableOpacity
                          onPress={() => UpdatePasswordVisiblity ()}
                          style={styles.eyeIcon}
                        >
                          {secureTextEntry ? Vector.EyeOff : Vector.Eye}
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                  <TouchableOpacity
                    style={styles.onPress}
                    onPress={() => signup (name, email, password)}
                  >
                    <Text style={styles.submit}>Submit</Text>
                  </TouchableOpacity>
                </View>
                <View style={styles.already}>
                  <Text style={styles.txt1}>Already have an accounts ? </Text>
                  <TouchableOpacity
                    onPress={() => navigation.navigate ('Login')}
                  >
                    <Text
                      style={{
                        color: COLORS.primaryColor,
                        textDecorationLine: 'underline',
                      }}
                    >
                      Login
                    </Text>
                  </TouchableOpacity>
                </View>
              </KeyboardAwareScrollView>
            </SafeAreaView>
          </ScrollView>}
    </>
  );
};

export default Signup;

const styles = ScaledSheet.create ({
  container: {
    flex: 1,
    backgroundColor: COLORS.greyBackgroundColour,
  },
  card: {
    marginTop: '20@vs',
    marginHorizontal: '20@s',
  },
  image: {
    height: '200@vs',
    width: '100%',
  },
  view: {
    alignSelf: 'center',
  },
  title: {
    ...FONTS.h2,
    color: COLORS.whiteColor,
    marginHorizontal: '20@s',
  },
  txt: {
    ...FONTS.body3,
    color: COLORS.whiteColor,
  },
  inputTxt: {
    ...FONTS.h4,
    height: '50@vs',
    width: '310@s',
    alignSelf: 'center',
    backgroundColor: COLORS.greyBackgroundColour,
    color: COLORS.whiteColor,
    borderColor: COLORS.lightGrey,
    marginTop: '5@vs',
    borderRadius: '6@msr',
    paddingStart: '10@msr',
  },
  errorTxt: {
    ...FONTS.body5,
    marginTop: '2@vs',
    color: COLORS.lightGrey,
  },
  txt1: {
    ...FONTS.body4,
    color: COLORS.whiteColor,
  },
  eyeIcon: {
    position: 'absolute',
    top: '18@vs',
    right: '20@s',
    zIndex: 1,
  },
  onPress: {
    height: '40@vs',
    width: '90@s',
    marginTop: '30@vs',
    alignSelf: 'flex-end',
    backgroundColor: COLORS.lightGrey,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '10@msr',
    marginEnd: '20@vs',
    marginBottom: '20@vs',
  },
  submit: {
    ...FONTS.h5,
    color: COLORS.greyBackgroundColour,
  },
  already: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginTop: '30@vs',
    marginBottom: '40@vs',
  },
});
