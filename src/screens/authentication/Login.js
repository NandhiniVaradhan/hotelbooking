import React, {useRef, useState} from 'react';
import {
  Text,
  View,
  SafeAreaView,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import auth from '@react-native-firebase/auth';
import {validateEmail, snackBar} from '../../utils';
import {COLORS, FONTS, Vector, Images} from '../../constants';
import Loading from '../../components/loader/Loading';

const Login = ({navigation}) => {
  const [email, setEmail] = useState ('');
  const [password, setPassword] = useState ('');
  const [secureTextEntry, setSecureTextEntry] = useState (true);
  const [loading, setLoading] = useState (false);

  const input1 = useRef ();
  const input2 = useRef ();

  const UpdatePasswordVisiblity = () => {
    setSecureTextEntry (!secureTextEntry);
  };

  const login = async (email, password, name) => {
    if (email === '') {
      snackBar ('E-mail address is required');
    } else if (!validateEmail (email)) {
      snackBar ('E-mail address is invalid');
    } else if (password === '') {
      snackBar ('Password is required');
    } else {
      setLoading (true);
      try {
        await auth ()
          .signInWithEmailAndPassword (email, password)
          .then (async res => {
            const update = {
              displayName: name,
            };
            await res.user.updateProfile (update);
            setLoading (false);
            snackBar ('Successfully, Logged in');
          });
      } catch (error) {
        if (error.code === 'auth/wrong-password') {
          snackBar ('E-mail address/Password is incorrect!');
        }
        if (error.code === 'auth/user-not-found') {
          snackBar ('E-mail is not registered!');
        }
      }
      setLoading (false);
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      {loading
        ? <Loading />
        : <KeyboardAwareScrollView
            keyboardShouldPersistTaps={'handled'}
            scrollEnabled={true}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{flexGrow: 1}}
          >
            <Image
              source={Images.signImg}
              style={{height: 200, width: 400}}
              resizeMode={'cover'}
            />
            <View>
              <Text style={[styles.title, {marginTop: 20}]}>User Login</Text>
              <View style={styles.view}>
                <View style={{marginTop: 5}}>
                  <Text style={styles.txt}>Email</Text>
                  <TextInput
                    style={styles.inputTxt}
                    onChangeText={text => setEmail (text)}
                    value={email}
                    placeholder="Enter Email"
                    placeholderTextColor={COLORS.darkGrey}
                    keyboardType="email-address"
                    autoCapitalize="none"
                    borderWidth={1}
                    returnKeyType="next"
                    onSubmitEditing={() => {
                      input2.current.focus ();
                    }}
                    blurOnSubmit={false}
                    ref={input1}
                  />
                </View>
                <View style={{marginTop: 20}}>
                  <Text style={styles.txt}>Password</Text>
                  <View style={{flexDirection: 'row'}}>
                    <TextInput
                      style={styles.inputTxt}
                      onChangeText={text => setPassword (text)}
                      value={password}
                      placeholder="Enter password"
                      placeholderTextColor={COLORS.darkGrey}
                      keyboardType="default"
                      borderWidth={1}
                      secureTextEntry={secureTextEntry ? true : false}
                      returnKeyType="done"
                      ref={input2}
                    />
                    <TouchableOpacity
                      onPress={() => UpdatePasswordVisiblity ()}
                      style={styles.eyeIcon}
                    >
                      {secureTextEntry ? Vector.EyeOff : Vector.Eye}
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              <TouchableOpacity
                style={styles.btn}
                onPress={() => login (email, password)}
              >
                <Text style={styles.btnTxt}>Submit</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.account}>
              {/* <View style={{flexDirection: 'row', marginTop: 10}}>
              <TouchableOpacity
                style={{
                  marginHorizontal: 5,
                  height: 40,
                  width: 40,
                  marginTop: 10,
                  backgroundColor: COLORS.primaryColor,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 20,
                  marginBottom: 20,
                }}
              >
                {Vector.FaceBook}
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  marginHorizontal: 5,
                  height: 40,
                  width: 40,
                  marginTop: 10,
                  backgroundColor: COLORS.lightGrey,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 20,
                  marginBottom: 20,
                }}
              >
                {Vector.Google}

              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  marginHorizontal: 5,
                  height: 40,
                  width: 40,
                  marginTop: 10,
                  backgroundColor: COLORS.lightGrey,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 20,
                  marginBottom: 20,
                }}
              >
                {Vector.Twitter}
              </TouchableOpacity>
            </View> */}
            </View>
            <View style={styles.txt2}>
              <Text style={styles.txt1}>Don't have an account? </Text>
              <TouchableOpacity onPress={() => navigation.navigate ('Signup')}>
                <Text style={styles.txt3}>Sign Up</Text>
              </TouchableOpacity>
            </View>
          </KeyboardAwareScrollView>}
    </SafeAreaView>
  );
};

export default Login;

const styles = ScaledSheet.create ({
  container: {
    flex: 1,
    backgroundColor: COLORS.greyBackgroundColour,
  },
  view: {
    marginTop: '20@vs',
    marginHorizontal: '20@s',
  },
  btnTxt: {
    ...FONTS.h5,
    color: COLORS.greyBackgroundColour,
  },
  btn: {
    height: '40@vs',
    width: '90@s',
    marginTop: '30@vs',
    alignSelf: 'flex-end',
    marginEnd: '20@s',
    backgroundColor: COLORS.lightGrey,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '10@msr',
  },
  title: {
    ...FONTS.h2,
    color: COLORS.whiteColor,
    marginHorizontal: '20@s',
  },
  txt: {
    ...FONTS.body3,
    color: COLORS.whiteColor,
  },
  txt1: {
    ...FONTS.body4,
    color: COLORS.whiteColor,
  },
  txt2: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginBottom: '40@vs',
  },
  txt3: {
    color: COLORS.primaryColor,
    textDecorationLine: 'underline',
  },
  inputTxt: {
    ...FONTS.h4,
    height: '50@vs',
    width: '310@s',
    alignSelf: 'center',
    backgroundColor: COLORS.greyBackgroundColour,
    color: COLORS.whiteColor,
    borderColor: COLORS.lightGrey,
    marginTop: '5@vs',
    borderRadius: '6@msr',
    paddingStart: '10@msr',
  },
  errorTxt: {
    ...FONTS.body5,
    marginTop: '2@vs',
    color: COLORS.lightGrey,
  },
  eyeIcon: {
    position: 'absolute',
    top: '18@vs',
    right: '20@s',
    zIndex: 1,
  },
  account: {
    alignItems: 'center',
    marginTop: '90@vs',
  },
});
