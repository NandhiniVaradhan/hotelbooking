import React, {useState, useEffect} from 'react';
import {View, StyleSheet} from 'react-native';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps
import getDirections from 'react-native-google-maps-directions';
import {Vector} from '../../constants';
import {CommonPopup} from '../../components/modal/CommonPopup';
import Loading from '../../components/loader/Loading';

const data = [
  {
    id: 1,
    image: 'https://media.gettyimages.com/photos/gopuram-of-tenkasi-temple-tamilnadu-india-asia-picture-id521908474?s=2048x2048',
    latitude: 10.956749,
    longitude: 79.381843,
    temple: 'Kasi Viswanathar Temple',
    details: 'Kasi Viswanathar temple is in the heart of the city and is very close to the famous Mahamaham Tank. As one of the top religious sites,it is dedicated to Lord Shiva.This 72 feet high temple was established in the 16th century. ',
  },
  {
    id: 2,
    image: 'https://www.oyorooms.com/travel-guide/wp-content/uploads/2019/03/Adi-Kumbeswara-Swami-Temple.jpg',
    latitude: 10.958782,
    longitude: 79.370382,
    temple: 'AdiKumbeswaraSwami Temple',
    details: 'Built during the Chola dynasty in the 9th century, this temple is dedicated to Lord Shiva, The Lingam here is broad at the bottom and narrow at the top in the shape of a needle. This Lingam is believed to have been made by Shiva himself.',
  },
  {
    id: 3,
    image: 'https://www.oyorooms.com/travel-guide/wp-content/uploads/2019/03/Airavathesvara-Temple.jpg',
    latitude: 10.948389,
    longitude: 79.356343,
    temple: 'AiravathesvaraTemple',
    details: 'This temple is one of the religious sites that are a part of the UNESCO World Heritage Site, built in the 12th century by Rajaraja Chola II. The deity’s consort, Periya Nayaki Amman, has her temple situated near this temple.',
  },
  {
    id: 4,
    image: 'https://www.oyorooms.com/travel-guide/wp-content/uploads/2019/03/Chakrapani-Temple.jpg',
    latitude: 10.963294,
    longitude: 79.372763,
    temple: 'chakrapani Temple',
    details: 'Devoted to Lord Vishnu, this temple is named as Chakrapani Lord Vishnu and appears in the form of a discus or Chakra.According to legend, Lord Vishnu sent Sudarshana Chakra to Patal Lok to kill the demon Jalandasura and have come out through river Cauvery. ',
  },
  {
    id: 5,
    image: 'https://www.oyorooms.com/travel-guide/wp-content/uploads/2019/03/Chakrapani-Temple.jpg',
    latitude: 10.958614,
    longitude: 79.378631,
    temple: 'Nageswaran Temple',
    details: 'Built by Aditya Chola in the 9th century, Nageswaran Temple is known for great architecture, building technology and astronomy.This temple is one of the religious sites dedicated to Lord Shiva, the serpent king. It is believed that the temple got its name Nageswaran.',
  },
  {
    id: 6,
    image: 'https://www.oyorooms.com/travel-guide/wp-content/uploads/2019/03/Ramaswamy-Temple.jpg',
    latitude: 10.959041,
    longitude: 79.385783,
    temple: 'MahalingaSwamy Temple',
    details: 'The Shiva Lingam here is a swayambhu (self-manifested) and therefore a large number of devotees can be seen here all year round. Shiva appeared in the form of a flame to please Agastya and other sages who were performing penance Jyothirmaya Mahalingam.',
  },
  {
    id: 7,
    image: 'https://www.oyorooms.com/travel-guide/wp-content/uploads/2019/03/Ramaswamy-Temple.jpg',
    latitude: 10.957061,
    longitude: 79.373488,
    temple: 'Ramaswamy Temple',
    details: 'Rama Navami is celebrated in a grand manner. This is also one of the five Vishnu temples that are connected with the Mahamaham festival.This temple is dedicated to Lord Rama, an incarnation of Lord Vishnu, constructed during the 16th century CE.',
  },
  {
    id: 8,
    image: 'https://www.oyorooms.com/travel-guide/wp-content/uploads/2019/03/Sarangapani-Temple.jpg',
    latitude: 10.9596649,
    longitude: 79.3755468,
    temple: 'Sarangapani Temple',
    details: 'The chariot festival held in the spring season is an important festival of this temple. Sarangapani Temple is the biggest Vishnu temple in Kumbakonam and third of the 108 Divya Desams.one of the top religious sites built by the Nayak Kings in the 16th century.',
  },
  {
    id: 9,
    image: 'https://media.gettyimages.com/photos/gopuram-of-tenkasi-temple-tamilnadu-india-asia-picture-id521908474?s=2048x2048',
    latitude: 10.956799,
    longitude: 79.387825,
    temple: 'Uppiliappan Temple',
    details: 'Lord Vishnu is worshipped as Uppiliappan and his consort Lakshmi as Bhoomidevi. You can see the 8 feet tall idol of the Lord with Bhoomidevi on the right and Markandeya Maharishi on the left. The lord is also known as Lord Venkateshwara’s brother.',
  },
  {
    id: 10,
    image: 'https://www.oyorooms.com/travel-guide/wp-content/uploads/2019/03/Suryanarayana-Temple.jpg',
    latitude: 10.953765,
    longitude: 79.383568,
    temple: 'Dhenupureeswarar Temple',
    details: 'It is one of the biggest temples and most important religious sites dedicated to Shiva but the most famous deity in this temple is Goddess Durga, a guardian deity of this temple. The temple is famous for many mythological stories.',
  },
];

const MapPage = () => {
  const [loading, setLoading] = useState (true);
  const [name, setName] = useState ('');
  const [image, setImage] = useState ('');
  const [detail, setDetail] = useState ('');
  const [lat, setLat] = useState ('');
  const [long, setLong] = useState ('');
  const [logoutAction, setLogoutAction] = useState (false);

  useEffect (() => {
    setLoading (false);
  }, []);

  const Popup = item => {
    setLogoutAction (true);
    setLat (item.latitude);
    setLong (item.longitude);
    setName (item.temple);
    setImage (item.image);
    setDetail (item.details);
  };
  const handleGetDirections = () => {
    const direction = {
      source: {
        latitude: 10.959815,
        longitude: 79.380898,
      },
      destination: {
        latitude: lat,
        longitude: long,
      },
    };
    getDirections (direction);
  };
  return (
    <View style={{flex: 1}}>
      {loading
        ? <Loading />
        : <View style={styles.container}>
            <MapView
              provider={PROVIDER_GOOGLE} // remove if not using Google Maps
              style={styles.map}
              zoomEnabled={true}
              maxZoomLevel={28}
              region={{
                latitude: 10.959815,
                longitude: 79.380898,
                latitudeDelta: 0.015,
                longitudeDelta: 0.0121,
              }}
            >
              <Marker
                coordinate={{latitude: 10.959815, longitude: 79.380898}}
                title={'Hotel Luxury'}
                description={'Current Location'}
              >
                {Vector.Hotel}
              </Marker>
              {data.map (item => (
                <Marker
                  key={item.id}
                  onPress={() => Popup (item)}
                  coordinate={{
                    latitude: item.latitude,
                    longitude: item.longitude,
                  }}
                  title={'nearby temple'}
                  description={item.temple}
                >
                  {Vector.Map}
                </Marker>
              ))}
            </MapView>
            <CommonPopup
              type={1}
              title={name}
              image={{uri: image}}
              subtitle={detail}
              isVisible={logoutAction}
              onClose={() => setLogoutAction (false)}
              onPress={() => handleGetDirections ()}
            />
          </View>}
    </View>
  );
};
export default MapPage;

const styles = StyleSheet.create ({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: '100%',
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    minHeight: 100,
  },
});
