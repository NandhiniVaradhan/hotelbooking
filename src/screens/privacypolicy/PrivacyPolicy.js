import React,{useState,useEffect} from 'react';
import {SafeAreaView, StyleSheet, View} from 'react-native';
import {WebView} from 'react-native-webview';
import Loading from '../../components/loader/Loading';
import { COLORS } from '../../constants';

const PrivacyPolicy = () => {

  const [loading, setLoading] = useState (true);

    useEffect (() => {
    setTimeout(() => {
     setLoading (false) 
    },1000);
  }, []);

  return (
      <SafeAreaView style={{flex: 1}} >
      {loading ? <Loading />:
      <View style={styles.container}>
        <WebView
          source={{
            uri: 'https://www.privacypolicygenerator.info/live.php?token=Vr3kH4czqFx5Vbe7yZd13NigrVx9vuha',
          }}
          hasZoom={false}
          scrollEnabled={false}
        />
      </View>}
    </SafeAreaView>
  );
};
const styles = StyleSheet.create ({
  container: {
    backgroundColor: COLORS.backgroundColour,
    flex: 1,
  },
});

export default PrivacyPolicy;
