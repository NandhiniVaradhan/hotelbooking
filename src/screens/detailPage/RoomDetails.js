import React, {useState} from 'react';
import {
  SafeAreaView,
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import DateTimePicker from '@react-native-community/datetimepicker';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Mailer from 'react-native-mail';
import {CommonPopup} from '../../components/modal/CommonPopup';
import {COLORS, FONTS, Vector, adjust} from '../../constants';
import CommonButton from '../../components/buttons/CommonButton';
import {RoomDescription} from './RoomDescription';
import {snackBar} from '../../utils';

const RoomDetails = ({route}) => {
  const {
    Room_Image,
    Room_Title,
    Room_Person,
    Room_Description,
  } = route.params;

  const [date1, setDate1] = useState (new Date ()); //calendar
  const [date2, setDate2] = useState (new Date ()); //calendar
  const [time1, setTime1] = useState(new Date()); // time
  const [time2, setTime2] = useState(new Date()); // time
  const [displayDate1, setDisplayDate1] = useState ('date'); //date
  const [displayDate2, setDisplayDate2] = useState ('date'); //date
  const [displayTime1, setDisplayTime1] = useState ('time'); //time
  const [displayTime2, setDisplayTime2] = useState ('time'); //time
  const [isDisplayDate1, setShow1] = useState (false); //date
  const [isDisplayDate2, setShow2] = useState (false); //date
  const [isDisplayTime1, setShowTime1] = useState (false); //time
  const [isDisplayTime2, setShowTime2] = useState (false); //time
  const [dateInput1, setDateInput1] = useState (''); //date
  const [dateInput2, setDateInput2] = useState (''); //date
  const [timeInput1, setTimeInput1] = useState (''); //time
  const [timeInput2, setTimeInput2] = useState (''); //time
  const [price, setPrice] = useState ('');
  const [disable, setDisable] = React.useState (true);
  const [logoutAction, setLogoutAction] = useState (false);
  const [action, setAction] = useState (false);

  const Date_Changer = () => {
    const dat1 = new Date (date1);
    const dat2 = new Date (date2);
    const diffTime = Math.abs (dat2 - dat1);
    const diffDays = Math.ceil (diffTime / (1000 * 60 * 60 * 24));
    const total = diffDays * 500;
    console.log (total + 'total');
    console.log (diffDays + ' days');
    setPrice (total);
  };

  const changeSelectedDate1 = (event, selectedDate) => {
    const currentDate = selectedDate || date1;
    // setDate1 (currentDate);
  selectedDate && setDate1(selectedDate);
    let tempDate = new Date (currentDate);
    let FDate =
      tempDate.getDate () +
      '/' +
      (tempDate.getMonth () + 1) +
      '/' +
      tempDate.getFullYear ();
    // let FTime = tempDate.toLocaleTimeString ();
    setDateInput1 (FDate);
    setShow1 (false);
  };

  const changeSelectedDate2 = (event, selectedDate) => {
    const currentDate = selectedDate || date2 ;
   setDate2 (currentDate);
   if(event.type == "set") { //ok button
    setDate2(currentDate)
   }
   else {
   return null;
}
  
// if (dateInput2 == '') {
// // setDate2(currentDate);
// setShow2(false);
// setDateInput2('');
// return;
// }else {
// // setShow2(false);
// setDateInput2(LDate);
// }


// console.log(event);
// if (event.type == "set") {
//   setDate2(currentDate);
//   return;
// }
// else if (event.type == "dismissed"){
//  setDate2('');
//  return;
// }
// setShow(Platform.OS === 'ios');
// setDate2(currentDate);
// if (dateInput2 !== undefined) {
//   setDate2(selectedDate);
// } else {
//   return; // setDate(selectedDate);
// }

    let tempDate = new Date (currentDate);
    let LDate =
      tempDate.getDate () +
      '/' +
      (tempDate.getMonth () + 1) +
      '/' +
      tempDate.getFullYear ();
    //let FTime = tempDate.toLocaleTimeString ();
    setDateInput2 (LDate);
    setShow2 (false);
    Date_Changer ();
    setDisable (false);
    console.log('dddddddddddddddd',LDate)
  };
  const changeSelectedTime1 = (event, selectedDate) => {
    const currentTime = selectedDate || time1;
      setTime1(currentTime);
      let tempTime = new Date (currentTime);
      let FTime = tempTime.toLocaleTimeString ();
      setTimeInput1 (FTime);
      setShowTime1 (false);
  }
  const changeSelectedTime2 = (event, selectedDate) => {
   const currentTime = selectedDate || time2;
      setTime2(currentTime);
      let tempTime = currentTime;
      let LTime = tempTime.toLocaleTimeString ();
      setTimeInput2 (LTime);
      setShowTime2 (false);
  }
  const showMode1 = currentMode => {
    setShow1 (true);
    setDisplayDate1 (currentMode);
  };
  const showMode2 = currentMode => {
    setShow2 (true);
    setDisplayDate2 (currentMode);

  };
  const showMode3 = currentMode => {
    setShowTime1(true);
    setDisplayTime1 (currentMode);
  };
  const showMode4 = currentMode => {
    setShowTime2 (true);
    setDisplayTime2 (currentMode);
  };
  const displayDatepicker1 = () => {
    showMode1 ('date');
  };
  
  const displayDatepicker2 = () => {
    {
      timeInput1.length !== 0
        ? showMode2 ('date')
        : snackBar ('Please Enter Check In Date & Time');
    }
    
  };
  const displayTimepicker1 = () => {
    {
      dateInput1.length !== 0
        ? showMode3 ('time')
        : snackBar ('Please Enter Check In Date');
    }
  };
  const displayTimepicker2 = () => {
    {
      dateInput2.length !== 0
        ? showMode4 ('time')
        : snackBar ('Please Enter Check out Date');
    }
  };

  const onRegister = () => {
    if (timeInput2.length !== 0) {
      handleEmail();
    } else {
      
    }
  };


  const handleEmail = () => {
    Mailer.mail (
      {
        subject: 'Hotel Room Amount',
        recipients: ['nandhinisiam@gmail.com'],
        body: `<p>Room Booking Detail :-</p>
               <p>CheckIn:-</p>
               <p>Date -> ${dateInput1} </p>
               <p>Time -> ${timeInput1} </p>
               <p>CheckOut:-</p>
               <p>Date -> ${dateInput2} </p>
               <p>Time -> ${timeInput2} </p>
               <br>
               <p>Total Amount - ₹ ${price}</p>`,
        isHTML: true,
      },
      (error, event) => {
        Alert.alert (
          error,
          event,
          [
            {
              text: 'Ok',
              onPress: () => console.log ('OK: Email Error Response'),
            },
            {
              text: 'Cancel',
              onPress: () => console.log ('CANCEL: Email Error Response'),
            },
          ],
          {cancelable: true}
        );
      }
    );
  };

  return (
    <ScrollView style={styles.container}>
      <SafeAreaView>
        <Image
          source={{uri: Room_Image}}
          style={styles.img}
          resizeMode={'cover'}
        />
        <View style={{marginHorizontal: 10}}>
          <TouchableOpacity onPress={() => setAction (true)}>
            <View style={styles.roomTxt}>
              <Text style={[styles.title, {color: COLORS.primaryColor}]}>
                {Room_Title}
              </Text>
              <View
                style={{justifyContent: 'center', marginTop: 6, marginStart: 5}}
              >
                {Vector.DownArrow}
              </View>
            </View>
          </TouchableOpacity>
          <Text style={[styles.header, {marginTop: 15}]}>Details:</Text>
          <View style={{flexDirection: 'row', marginTop: 5}}>
            <View style={{width: 25}}>
              {Vector.Food}
            </View>
            <Text style={[styles.text2, {marginTop: 5}]}>
              Breakfast included
            </Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 2}}>
            <View style={{width: 25}}>
              {Vector.Wifi}
            </View>
            <Text style={styles.text2}>Free Wi-Fi</Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 2}}>
            <View style={{width: 25}}>
              {Vector.Parking}
            </View>
            <Text style={styles.text1}>Parking</Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 2}}>
            <View style={{width: 25}}>
              {Vector.Water}
            </View>
            <Text style={styles.text1}>Drinking water</Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 2}}>
            <View style={{width: 25}}>
              {Vector.Person}
            </View>
            <Text style={styles.text1}>{Room_Person}</Text>
          </View>
        </View>

        <View style={styles.inOut}>

          <View style={styles.checkView}>
            <Text style={styles.checkTxt}>
              Check In
            </Text>
            <View style={styles.card2} >
              <View style={styles.date}>
                <Text style={styles.pickedDate}>
                  {dateInput1}
                </Text>
                <Text style={styles.pickedTime}>
                  {timeInput1}
                </Text>
              </View>
              <View style={{flexDirection: 'column'}}>
               {displayDate1 &&
                <TouchableOpacity style={styles.icon} onPress={displayDatepicker1}>
                  <Ionicons
                    name="calendar"
                    size={20}
                    color={COLORS.blackColor}
                    style={{alignSelf: 'flex-end'}}
                  />
                </TouchableOpacity>}
                {displayTime1 &&
                <TouchableOpacity style={styles.clockIcon} onPress={displayTimepicker1}>
                  {Vector.Clock}
                </TouchableOpacity>}
              </View>
            </View>
          </View>
          {isDisplayDate1 &&
            <DateTimePicker
              value={date1}
              mode="date"
              is24Hour={true}
              display="calendar"
              onChange={changeSelectedDate1}
              style={styles.datePicker}
              onTouchCancel={isDisplayDate1}
              minimumDate={new Date ()}
            />}
          {isDisplayTime1 && (
            <DateTimePicker
               value={time2}
               mode="time"
               is24Hour={true}
               display="clock"
               onTouchCancel={isDisplayTime1}
               onChange={changeSelectedTime1} 
              style={styles.datePicker}
            />
         )}

          <View style={styles.checkView}>
            <Text style={styles.checkTxt}>
              Check Out
            </Text>
            <View
              style={styles.card2} 
            >
              <View style={styles.date}>
                <Text style={styles.pickedDate}>
                  {dateInput2}
                </Text>
                <Text style={styles.pickedTime}>
                  {timeInput2}
                </Text>
              </View>
             <View style={{flexDirection:'column'}}>
              {displayDate2 &&
                <TouchableOpacity style={styles.icon} onPress={dateInput1 !== null ? displayDatepicker2 : null}>
                  <Ionicons
                    name="calendar"
                    size={20}
                    color={COLORS.blackColor}
                    style={{alignSelf: 'flex-end'}}
                  />
                </TouchableOpacity>}
                {displayTime2 &&
                <TouchableOpacity style={styles.clockIcon} onPress={displayTimepicker2}>
                  {Vector.Clock}
                </TouchableOpacity>}
                </View>
            </View>
          </View>
        </View>

        {isDisplayDate2 &&
          <DateTimePicker
            value={date2}
            mode="date"
            is24Hour={true}
            display="calendar"
            onTouchCancel={isDisplayDate2}
            onChange={changeSelectedDate2}
            style={styles.datePicker}
            minimumDate={new Date (date1)}
          />}
        {isDisplayTime2 && (
            <DateTimePicker
               value={time2}
               mode="time"
               is24Hour={true}
               display="clock"
               onTouchCancel={isDisplayTime2}
               onChange={changeSelectedTime2}  
               style={styles.datePicker}
            />
         )}

        <View style={styles.flatListParentView} />
        <View style={styles.totalPrice}>
          <Text style={[styles.header, {marginTop: 5}]}>Total price</Text>
          <View>
            <Text style={styles.header}>₹{timeInput2.length !== 0 ? price : '0'}</Text>
            <Text style={styles.text1}>with taxes & fees</Text>
          </View>
        </View>
        <View style={styles.flatListParentView} />
        <View
          style={{marginVertical: 20, alignItems: 'flex-end', marginEnd: 20}}
        >
          <CommonButton
            buttonStyle={disable ? styles.disableStyle : styles.ButtonStyle_1}
            isLoading={false}
            text={'Register'}
            textStyle={styles.buttonTextStyle_1}
            onPress={onRegister}
            disable={disable ? false : true}
          />
        </View>
        <CommonPopup
          type={0}
          title={'Room Booking'}
          subtitle={'Your Total Amount - $'}
          isVisible={logoutAction}
          total={price}
          successButtonText={'Yes'}
          negativeButtonText={'No'}
          onPressNo={() => setLogoutAction (false)}
          onPressYes={() => handleEmail ()}
        />
        <RoomDescription
          title={'Room Description'}
          subtitle={Room_Description}
          isVisible={action}
          onClose={() => setAction (false)}
        />
      </SafeAreaView>
    </ScrollView>
  );
};

export default RoomDetails;

const styles = ScaledSheet.create ({
  container: {
    flex: 1,
    backgroundColor: COLORS.backgroundColour,
  },
  img: {
    height: '230@vs',
    width: '400@s',
  },
  roomTxt: {
    flexDirection: 'row',
    marginTop: '10@vs',
  },
  buttonTextStyle_1: {
    ...FONTS.P0,
    fontSize: adjust (12),
    color: COLORS.whiteColor,
  },
  ButtonStyle_1: {
    height: '40@vs',
    width: '135@s',
    borderRadius: adjust (5),
    backgroundColor: COLORS.primaryColor,
  },
  buttonTextStyle_2: {
    ...FONTS.P0,
    fontSize: adjust (12),
    color: COLORS.blackColor,
  },
  ButtonStyle_2: {
    height: '40@vs',
    width: '135@s',
    borderRadius: adjust (5),
    backgroundColor: COLORS.secondaryColor,
  },
  title: {
    ...FONTS.h2,
    color: COLORS.blackColor,
  },
  header: {
    ...FONTS.h3,
    color: COLORS.blackColor,
  },
  text1: {
    ...FONTS.body5,
    color: COLORS.darkGrey,
  },
  text2: {
    ...FONTS.body5,
    color: COLORS.primaryColor,
  },
  total: {
    ...FONTS.h1,
    color: COLORS.blackColor,
  },
  flatListParentView: {
    flexDirection: 'row',
    paddingVertical: '10@vs',
    marginHorizontal: '10@s',
    borderBottomWidth: 0.6,
    borderBottomColor: COLORS.lightGrey,
  },
  pickedDateContainer: {
    paddingVertical: '10@vs',
  },
  pickedDate: {
    fontWeight: '500',
    color: COLORS.blackColor,
  },
  pickedTime: {
    fontWeight: '500',
    paddingTop: '8@msr',
    color: COLORS.blackColor,
  },
  datePicker: {
    width: '320@s',
    height: '260@vs',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  Dob: {
    height: '40@vs',
    width: '320@s',
    paddingStart: '10@s',
    borderWidth: 1,
    borderColor: COLORS.secondaryColor,
    borderRadius: '5@msr',
    alignSelf: 'center',
  },
  totalPrice: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: '10@msr',
    marginTop: '15@vs',
  },
  icon: {
    flex: 0.26,
    justifyContent: 'center',
    alignSelf: 'center',
  },
  clockIcon: {
    flex: 0.26,
    justifyContent: 'center',
    alignSelf: 'center',
    paddingTop: '5@msr',
  },
  date: {
    flex: 0.7,
    justifyContent: 'center',
    alignItems: 'center',
  },
  card2: {
    flex: 1,
    flexDirection: 'row',
    justifyContent:'space-evenly',
    backgroundColor: COLORS.secondaryColor,
    borderBottomRightRadius: '6@msr',
    borderBottomLeftRadius: '6@msr',
    paddingVertical: '8@msr',
  },
  checkTxt: {
    color: COLORS.whiteColor,
    ...FONTS.body3,
    alignSelf: 'center',
    paddingVertical: '5@msr',
  },
  checkView: {
    flex: 0.4,
    backgroundColor: COLORS.primaryColor,
    justifyContent: 'center',
    borderRadius: '6@msr',
  },
  inOut: {
    flexDirection: 'row',
    marginTop: '20@vs',
    justifyContent: 'space-evenly',
  },
  disableStyle: {
    height: '40@vs',
    width: '135@s',
    borderRadius: adjust (5),
    backgroundColor: COLORS.lightGrey,
  },
});




