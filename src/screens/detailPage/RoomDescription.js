import React from 'react';
import {Text, View, Dimensions, Image, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import {ScaledSheet} from 'react-native-size-matters';
import {FONTS, COLORS} from '../../constants/Theme';
import {Vector} from '../../constants';

var {width} = Dimensions.get ('window');

export const RoomDescription = ({isVisible, title, subtitle, onClose}) => {
  return (
    <Modal
      animationIn={'slideInUp'}
      isVisible={isVisible}
      style={styles.container}
    >
      <View style={styles.containerStyle}>
        <Text style={styles.order}>{title}</Text>
        <TouchableOpacity style={styles.icon} onPress={onClose}>
          {Vector.CancelX}
        </TouchableOpacity>
        <View style={{justifyContent: 'center', marginTop: 5}}>
          <Text style={styles.order_1}>{subtitle}</Text>
        </View>
      </View>
    </Modal>
  );
};
const styles = ScaledSheet.create ({
  containerStyle: {
    width: width - 30,
    backgroundColor: 'white',
    borderRadius: '4@msr',
    justifyContent: 'center',
    alignSelf: 'center',
    paddingVertical: '12@msr',
  },
  container: {
    height: '600@vs',
  },
  order: {
    ...FONTS.h3,
    fontWeight: '700',
    paddingHorizontal: '10@msr',
    paddingVertical: '5@vs',
    color: COLORS.blackColor,
  },
  order_1: {
    ...FONTS.body3,
    paddingTop: '5@msr',
    paddingHorizontal: '10@msr',
    color: COLORS.blackColor,
  },
  icon: {
    position: 'absolute',
    top: 10,
    right: 10,
  },
});
