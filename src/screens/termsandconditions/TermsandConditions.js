import React, {useState, useEffect} from 'react';
import {SafeAreaView, StyleSheet, View} from 'react-native';
import {WebView} from 'react-native-webview';
import Loading from '../../components/loader/Loading';
import {COLORS} from '../../constants';

const TermsandConditions = () => {
  const [loading, setLoading] = useState (true);

  useEffect (() => {
    setTimeout (() => {
      setLoading (false);
    }, 1000);
  }, []);

  return (
    <SafeAreaView style={{flex: 1}}>
      {loading
        ? <Loading />
        : <View style={styles.container}>
            <WebView
              source={{
                uri: 'https://www.termsandconditionsgenerator.com/live.php?token=fmmhnu10DPf1PJmxTH7t0wk9RDeDLQGH',
              }}
              hasZoom={false}
              scrollEnabled={false}
            />
          </View>}
    </SafeAreaView>
  );
};
const styles = StyleSheet.create ({
  container: {
    backgroundColor: COLORS.backgroundColour,
    flex: 1,
  },
});

export default TermsandConditions;
