import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  TextInput,
  Image,
  TouchableOpacity,
  Dimensions,
  FlatList,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import database from '@react-native-firebase/database';
import {COLORS, Vector, adjust, FONTS} from '../../constants';

var height = Dimensions.get ('window').height;
var width = Dimensions.get ('window').width;

const Data = [
  {
    image: 'https://th.bing.com/th/id/R.298a6cef0369a2a830128eef00b5fdad?rik=oRyFvhcHD2DD6A&riu=http%3a%2f%2fwww.hotelanna.fi%2fwp-content%2fuploads%2f2017%2f04%2fMJP-1659-Edit.jpg&ehk=szLYUuM3rJn6mW0b7SBcggYAxmlCf5IOl7U9UJ9qiFo%3d&risl=&pid=ImgRaw&r=0',
    title: 'Single Room',
    price: 'Rs. 3000 ',
    type: 'Deluxe Room Queen',
  },
  {
    image: 'https://media-cdn.tripadvisor.com/media/photo-s/0b/4d/38/23/bedroom.jpg',
    title: 'Single Room',
    price: 'Rs. 3000 ',
    type: 'Luxury Queen',
  },
  {
    image: 'https://images.trvl-media.com/hotels/2000000/1540000/1534300/1534252/1953db02_z.jpg',
    title: 'Couple Room',
    price: 'Rs. 8000 ',
    type: 'Studio Suite',
  },
  {
    image: 'https://res.cloudinary.com/lastminute/image/upload/c_scale,w_630/q_auto/v1584701860/kjvgsbymcqkqhvb0p61q.jpg',
    title: 'Couple Room',
    price: 'Rs. 8000 ',
    type: 'Deluxe Room Queen',
  },
  {
    image: 'https://www.wyndhamhotels.com/content/dam/property-images/en-us/lq/us/tx/lubbock/53211/53211_guest_room_2.jpg?downsize=1200:*',
    title: 'Couple Room',
    price: 'Rs. 8000 ',
    type: 'Luxury Queen',
  },
  {
    image: 'https://flightcentre-us.leonardocontentcloud.com/imageRepo/5/0/90/694/627/TPABD_4209570945_P.jpg',
    title: 'Couple Room',
    price: 'Rs. 8000 ',
    type: 'Luxury Twin',
  },
  {
    image: 'https://media-cdn.tripadvisor.com/media/photo-s/04/b0/25/1e/kymi-palace.jpg',
    title: 'Couple Room',
    price: 'Rs. 8000 ',
    type: 'Deluxe Suite',
  },
  {
    image: 'https://www.taiwanhotelnews.com/uploads/7/6/8/3/76832353/8385524_orig.jpg',
    title: 'Couple Room',
    price: 'Rs. 8000 ',
    type: 'Studio Suite',
  },
  {
    image: 'https://media-cdn.tripadvisor.com/media/photo-s/05/d1/ea/63/loft-suite.jpg',
    title: 'Family Room',
    price: 'Rs. 5000 ',
    type: 'Luxury Twin',
  },
  {
    image: 'https://az712897.vo.msecnd.net/images/full/5e0b59fa-9e5f-48bd-85e6-3397b7a2651e.jpeg',
    title: 'Family Room',
    price: 'Rs. 5000 ',
    type: 'Luxury Queen',
  },
  {
    image: 'https://ui.cltpstatic.com/places/hotels/3516/351613/images/b0c09c5d_w.jpg',
    title: 'Family Room',
    price: 'Rs. 5000 ',
    type: 'Deluxe Room Queen',
  },
  {
    image: 'https://images.trvl-media.com/hotels/1000000/30000/28100/28082/941ef85a_z.jpg',
    title: 'Family Room',
    price: 'Rs. 5000 ',
    type: 'Luxury Twin',
  },
  {
    image: 'https://media.iceportal.com/100252/photos/63450389_XXL.jpg',
    title: 'Family Room',
    price: 'Rs. 5000 ',
    type: 'Deluxe Suite',
  },
];

const Search = ({navigation}) => {
  // const [single, setSingle] = useState ([]);

  const searchHandle = () => {
    setArray ({array: ''});
  };

  // useEffect (() => {
  //   itemsRef.once ('value', snapshot => {
  //     let data = snapshot.val ();
  //     const Rooms = Object.values (data);
  //     setSingle (Rooms);
  //   });
  // }, []);

  // const searchFunction = array => {
  //   if (array) {
  //     const newData = Data.filter (function (item) {
  //       const itemData = item.title;
  //       const textData = array;
  //       return itemData.indexOf (textData) > -1;
  //     });
  //     setFilteredData (newData);
  //     setArray (array);
  //   } else {
  //     setFilteredData (Data);
  //     setArray (array);
  //   }
  // };
  // const search = array => {
  //   setArray (array);
  //   searchFunction (array);
  //   console.log ('array', array);
  // };

  const searchFilterFunction = text => {
    const textData = text;
    const filteredData = Data.filter (function (item) {
      return contains (item, textData);
    });
    setFilteredData (filteredData);
    setArray (text);
  };
  const contains = ({title, type}, search) => {
    if (title.includes (search) || type.includes (search)) {
      return true;
    }
    return false;
  };

  // let itemsRef = database ().ref (`/Rooms`);

  const [array, setArray] = React.useState ([]);
  const [filteredData, setFilteredData] = React.useState (Data);
  const flatItem = ({item}) => {
    return (
      <View style={styles.set}>
        <View style={styles.card}>
          <TouchableOpacity
            onPress={() =>
              navigation.navigate ('RoomDetails', {
                Room_Image: item.image,
                Room_Title: item.title,
                Room_Person: item.person,
              })}
          >
            <Image source={{uri: item.image}} style={styles.image} />
          </TouchableOpacity>
          <Text style={styles.text1}>{item.title}</Text>
          <Text style={styles.title3}>{item.type}</Text>
          <Text style={styles.text2}>{item.price}</Text>
        </View>
      </View>
    );
  };
  return (
    <SafeAreaView style={{flex: 1}}>
      <View>
        <View style={{justifyContent: 'center'}}>
          <TextInput
            style={styles.input}
            placeholder={'Search for Rooms...'}
            placeholderTextColor={COLORS.lightGrey}
            onChangeText={searchFilterFunction}
            value={array}
          />
          {/* <TouchableOpacity
            style={styles.icon}
            onPress={searchHandle}
            activeOpacity={0.6}
          >
            {Vector.CancelX}
          </TouchableOpacity> */}
        </View>

        <View style={{paddingBottom: 135}}>
          <FlatList
            data={filteredData}
            renderItem={flatItem}
            keyExtractor={(item, index) => index.toString ()}
            numColumns={2}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = ScaledSheet.create ({
  input: {
    backgroundColor: COLORS.whiteColor,
    color: COLORS.blackColor,
    borderRadius: '10@msr',
    height: '40@vs',
    width: width * 0.9,
    alignSelf: 'center',
    position: 'relative',
    marginVertical: '10@vs',
    fontSize: adjust (11),
    paddingLeft: '10@msr',
    paddingRight: '35@msr',
    elevation: 5,
    shadowOpacity: 0.7,
  },
  title3: {
    color: COLORS.blackColor,
    fontSize: adjust (10),
    marginStart: '10@s',
  },
  icon: {
    position: 'absolute',
    right: '30@s',
    backgroundColor: COLORS.whiteColor,
  },
  buttonStyle: {
    height: '35@vs',
    width: '88@s',
    borderRadius: '10@msr',
    backgroundColor: COLORS.primaryColor,
  },
  buttonTextStyle: {
    ...FONTS.h4,
    color: COLORS.whiteColor,
    marginBottom: '5@msr',
  },
  set: {
    marginTop: '5@vs',
    alignSelf: 'center',
  },
  image: {
    height: height * 0.2,
    width: width * 0.4,
    marginRight: '10@s',
    borderTopLeftRadius: '10@msr',
    borderTopRightRadius: '10@msr',
  },
  card: {
    backgroundColor: COLORS.whiteColor,
    width: width * 0.4,
    borderRadius: '10@msr',
    height: height * 0.32,
    marginLeft: '25@msr',
    marginTop: '20@msr',
  },
  text1: {
    ...FONTS.h4,
    color: COLORS.blackColor,
    marginTop: '5@vs',
    marginLeft: '10@s',
  },
  text2: {
    ...FONTS.h5,
    color: COLORS.blackColor,
    marginLeft: '10@s',
    marginTop: '5@vs',
  },
});
export default Search;
